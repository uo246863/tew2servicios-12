package impl.tew.persistence;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tew.model.Publicacion;
import com.tew.persistence.PublicacionDao;
import com.tew.persistence.exception.PersistenceException;

public class PublicacionJdbcDao implements PublicacionDao {
	
	
	
	public List<Publicacion> getPublicaciones() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		List<Publicacion> Publicaciones = new ArrayList<Publicacion>();

		try {
			// En una implemenntaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos. 
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement("select * from Publicacion");
			rs = ps.executeQuery();

			while (rs.next()) {
				
				Publicacion Publicacion = new Publicacion();
				Publicacion.setEmail(rs.getString("EMAIL"));
				Publicacion.setTexto(rs.getString("TITULO"));
				Publicacion.setTitulo(rs.getString("TEXTO"));
				Publicacion.setFecha(rs.getString("FECHA"));
				
				
				Publicaciones.add(Publicacion);
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally  {
			if (rs != null) {try{ rs.close(); } catch (Exception ex){}};
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
		
		return Publicaciones;
	}

	public List<Publicacion> getMisPublicaciones(String email) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		List<Publicacion> Publicaciones = new ArrayList<Publicacion>();
		
		try {
			// En una implemenntaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos. 
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement("select * from publicacion where email='"+email+"'");
			rs = ps.executeQuery();

			while (rs.next()) {
				
				Publicacion Publicacion = new Publicacion();
				Publicacion.setEmail(rs.getString("EMAIL"));
				Publicacion.setTexto(rs.getString("TITULO"));
				Publicacion.setTitulo(rs.getString("TEXTO"));
				Publicacion.setFecha(rs.getString("FECHA"));
				
				
				Publicaciones.add(Publicacion);
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally  {
			if (rs != null) {try{ rs.close(); } catch (Exception ex){}};
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
		
		return Publicaciones;
	}
	
	public List<Publicacion> getPublicacionesAmigos(String email) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		List<Publicacion> Publicaciones = new ArrayList<Publicacion>();
		
		try {
			// En una implemenntaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos. 
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			
			ps = con.prepareStatement("select * from publicacion where email in (select email_amigo from amigos where email_usuario='"+email+"') order by email");
			rs = ps.executeQuery();

			while (rs.next()) {
				
				Publicacion Publicacion = new Publicacion();
				Publicacion.setEmail(rs.getString("EMAIL"));
				Publicacion.setTexto(rs.getString("TITULO"));
				Publicacion.setTitulo(rs.getString("TEXTO"));
				Publicacion.setFecha(rs.getString("FECHA"));
				
				
				Publicaciones.add(Publicacion);
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally  {
			if (rs != null) {try{ rs.close(); } catch (Exception ex){}};
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
		
		return Publicaciones;
	}
	
	@Override
	public void crearPublicacion(String titulo, String texto, String email) {
		

		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		
		try{
		
		//Variables necesarias para usar la BD:
		String SQL_DRV = "org.hsqldb.jdbcDriver";
		String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";
		Class.forName(SQL_DRV);
		con = DriverManager.getConnection(SQL_URL, "sa", "");
		java.util.Date fechaActual = new java.util.Date();
		String fechaTabla= new SimpleDateFormat("yyyy-MM-dd").format(fechaActual);	
		
		String mensaje2="insert into Publicacion (email, titulo, texto, fecha) " +
			"values (?, ?, ?, ?)";
		System.out.println(email);
		ps=con.prepareStatement(mensaje2);
		ps.setString(1, email);	
		ps.setString(2, titulo);
		ps.setString(3, texto);
		ps.setString(4, fechaTabla);
		

		

		ps.executeUpdate();
		
		
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
	}
	
	@Override
	public void borrarPublicacion(String texto, String titulo, String fecha, String email){
		
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		
		try{
		
		//Variables necesarias para usar la BD:
		String SQL_DRV = "org.hsqldb.jdbcDriver";
		String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";
		Class.forName(SQL_DRV);
		con = DriverManager.getConnection(SQL_URL, "sa", "");
		ps = con.prepareStatement("delete from publicacion where texto='"+texto+"' AND titulo='"+titulo+"' AND fecha='"+fecha+"' AND email='"+email+"'");
		ps.executeUpdate();
		
		
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
	}
}
