package impl.tew.persistence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.*;

import org.jboss.resteasy.client.ClientRequest;


import org.json.simple.JSONValue;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;

import com.tew.model.Usuario;
import com.tew.persistence.UsuarioDao;
import com.tew.persistence.exception.*;



public class UsuarioJdbcDao implements UsuarioDao {

	public List<Usuario> getUsuarios() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		List<Usuario> Usuarios = new ArrayList<Usuario>();

		try {
			// En una implemenntaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos. 
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement("select * from Usuarios");
			rs = ps.executeQuery();

			while (rs.next()) {
				
				Usuario Usuario = new Usuario();
				Usuario.setEmail(rs.getString("EMAIL"));
				Usuario.setNombre(rs.getString("NOMBRE"));
				Usuario.setPasswd(rs.getString("PASSWD"));
				Usuario.setRol(rs.getString("ROL"));
				
				
				Usuarios.add(Usuario);
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally  {
			if (rs != null) {try{ rs.close(); } catch (Exception ex){}};
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
		
		return Usuarios;
	}
	
	@Override
	public void crearPublicacion(String titulo, String texto, String email) {
		
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		
		try{
		
		//Variables necesarias para usar la BD:
		String SQL_DRV = "org.hsqldb.jdbcDriver";
		String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";
		Class.forName(SQL_DRV);
		con = DriverManager.getConnection(SQL_URL, "sa", "");
		java.util.Date fechaActual = new java.util.Date();
		String fechaTabla= new SimpleDateFormat("yyyy-MM-dd").format(fechaActual);	
		
		String mensaje2="insert into Publicacion (email, titulo, texto, fecha) " +
			"values (?, ?, ?, ?)";
		System.out.println(email);
		ps=con.prepareStatement(mensaje2);
		ps.setString(1, email);	
		ps.setString(2, titulo);
		ps.setString(3, texto);
		ps.setString(4, fechaTabla);
		
		System.out.println(mensaje2);
		

		ps.executeUpdate();
		
		
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
	}
	
	@SuppressWarnings("resource")
	@Override 
	public void reiniciarBD(){
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		
		try{
		
		//Variables necesarias para usar la BD:
		String SQL_DRV = "org.hsqldb.jdbcDriver";
		String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";
		Class.forName(SQL_DRV);
		con = DriverManager.getConnection(SQL_URL, "sa", "");
		
		//Borramos la información teniendo en cuenta que no se debe borrar al administrador si es el que está reiniciando la base de datos:
		
		ps = con.prepareStatement("drop table amigos");
		ps.executeUpdate();
		ps = con.prepareStatement("drop table publicacion");
		ps.executeUpdate();
		ps = con.prepareStatement("delete from usuarios where rol!='admin'");
		ps.executeUpdate();
		
		
		//Pedimos el archivo json:
		ClientRequest cr = new ClientRequest("http://localhost:8180/tew2Servicios-12/redsocial.json");
		String result;
		try {
			
			//Recogemos el JSON en un string:
			result = cr.get(String.class).getEntity(String.class);
			
			//Cogemos los distintos arrays que serán nuestras tablas de la BD:
			JSONObject jsonObject = new JSONObject(result);
			JSONArray usuarios = jsonObject.getJSONArray("usuarios");
			JSONArray publis = jsonObject.getJSONArray("publicaciones");
			JSONArray amigos = jsonObject.getJSONArray("amigos");
			
			
		    //Y recorremos todos los usuarios contenidos, sacando a su vez los cuatro datos que introduciremos en la BD:
			for(int i=0;i<usuarios.length();i++){
				JSONObject usu = usuarios.getJSONObject(i);
				String nombre = usu.getString("nombre");
				String email = usu.getString("email");
				String rol = usu.getString("rol");
				String passwd = usu.getString("passwd");
				
				//Y los insertamos: 
				ps = con.prepareStatement("INSERT INTO USUARIOS VALUES('"+email+"','"+passwd+"','"+rol+"','"+nombre+"')");
				ps.executeUpdate();
				
				//Consulta para comprobar si hay un administrador en la base de datos (que debería de haber siempre):
				ps = con.prepareStatement("SELECT * FROM USUARIOS WHERE ROL='admin'");
				rs = ps.executeQuery();
				//Si está llamada devuelve falso quiere decir que la consulta ha retornado vacía y que, por tanto, no hay un admin:
				boolean adminesta = rs.next();
				//Si no hay, añadimos uno:
				if (!adminesta){
					ps = con.prepareStatement("INSERT INTO USUARIOS VALUES('admin@dominio.com','admin','admin','Jose')");
					ps.executeUpdate();
				}
				
			}
			
			
			//Creamos la tabla Publicaciones con sus respectivos campos y claves: 
			ps = con.prepareStatement("CREATE TABLE Publicacion (ID int  IDENTITY PRIMARY KEY, email varchar(30) NOT NULL, titulo varchar(20) NOT NULL, texto varchar(100), fecha date NOT NULL, FOREIGN KEY (email) REFERENCES Usuarios(email))"); 
			ps.executeUpdate();
			
			//Volvemos a sacar los datos igual que hicimos antes:
			for(int i=0;i<publis.length();i++){
				JSONObject pu = publis.getJSONObject(i);
				String titulo = pu.getString("titulo");
				String email = pu.getString("email");
				String texto = pu.getString("texto");
				String fecha = pu.getString("fecha");
				Date date =new Date(Integer.parseInt(fecha) * 1000L);
			
				//Y los insertamos: 				
				String insert1="insert into Publicacion (email, titulo, texto, fecha) " + "values (?, ?, ?, ?)";
				ps=con.prepareStatement(insert1);
				ps.setString(1, email);	
				ps.setString(2, titulo);
				ps.setString(3, texto);
				ps.setDate(4, date);
				ps.executeUpdate();				
				
			}
			
			//Por último, repetimos el proceso para la tabla de Amigos: 
			ps = con.prepareStatement("CREATE TABLE Amigos (email_usuario varchar(30), email_amigo varchar(30), aceptada bit NOT NULL, PRIMARY KEY (email_usuario, email_amigo), FOREIGN KEY (email_usuario) REFERENCES Usuarios(email), FOREIGN KEY (email_amigo) REFERENCES Usuarios(email))"); 
			ps.executeUpdate();	
			
			for(int i=0;i<amigos.length();i++){
				JSONObject ami = amigos.getJSONObject(i);
				String emailusu = ami.getString("emailusuario");
				String emailamigo = ami.getString("emailamigo");
				
				//Y los insertamos:
				String insert2 = "insert into Amigos (email_usuario, email_amigo, aceptada) " + "values (?, ?, ?)";
				ps = con.prepareStatement(insert2);
				ps.setString(1, emailusu);
				ps.setString(2, emailamigo);
				ps.setBoolean(3, true);
				ps.executeUpdate();
								
				
							
				
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
		
	}
	
	
	@Override
	public boolean delete(String email) throws NotPersistedException {
		PreparedStatement ps1 = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		Connection con = null;
		int rows, rows2, rows1 = 0;
		
		try {
			// En una implementaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos.
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement("delete from Amigos where email_usuario = ? or email_amigo = ?");
			
			ps.setString(1, email);
			ps.setString(2, email);

			rows = ps.executeUpdate();
			
			
			ps1 = con.prepareStatement("delete from Publicacion where email = ?");
			ps1.setString(1, email);
			rows1 = ps1.executeUpdate();
			
			ps2 = con.prepareStatement("delete from Usuarios where email = ?");
			ps2.setString(1, email);
			rows2 = ps2.executeUpdate();
			if (rows2 != 1) {
				throw new NotPersistedException("Usuario " + email + " not found");
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
		finally  {
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (ps1 != null) {try{ ps1.close(); } catch (Exception ex){}};
			if (ps2 != null) {try{ ps2.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
		
		return true;
	}

	@Override
	public Usuario findById(String email) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		Usuario Usuario1 = null;
		
		try {
			// En una implementaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos.
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement("select * from Usuarios where email = ?");
			ps.setString(1, email);
			
			rs = ps.executeQuery();
			Usuario1 = new Usuario();
			if (rs.next()) {
				Usuario1.setEmail(rs.getString("EMAIL"));
				Usuario1.setNombre(rs.getString("NOMBRE"));
				Usuario1.setPasswd(rs.getString("PASSWD"));
				Usuario1.setRol(rs.getString("ROL"));
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
		finally  {
			if (rs != null) {try{ rs.close(); } catch (Exception ex){}};
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
		
		return Usuario1;
	}

	@Override
	public void save(Usuario a) throws AlreadyPersistedException {
		PreparedStatement ps = null;
		Connection con = null;
		int rows = 0;
		
		try {
			// En una implementaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos.
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement(
					"insert into Usuarios (email, passwd, rol, nombre) " +
					"values (?, ?, ?, ?)");
			
			ps.setString(1, a.getEmail());
			ps.setString(2, a.getPasswd());
			ps.setString(3, "user");
			ps.setString(4, a.getNombre());

			rows = ps.executeUpdate();
			if (rows != 1) {
				throw new AlreadyPersistedException("Usuario " + a + " already persisted");
			} 

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
		finally  {
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
	}
	
	@Override
	public void editNombre(String email, String nombre) {
		PreparedStatement ps = null;
		Connection con = null;
		
		try {
			// En una implementaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos.
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement(
					"update Usuarios " +
					"set nombre = ?" +
					"where email = ?");
			
			ps.setString(1, nombre);
			ps.setString(2, email);
			ps.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
		finally  {
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
		
	}

	@Override
	public void editRol(String email, String rol) {
		PreparedStatement ps = null;
		Connection con = null;
		
		try {
			// En una implementaci��n m��s sofisticada estas constantes habr��a 
			// que sacarlas a un sistema de configuraci��n: 
			// xml, properties, descriptores de despliege, etc 
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos.
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement(
					"update Usuarios " +
					"set rol = ?" +
					"where email = ?");
			
			ps.setString(1, rol);
			ps.setString(2, email);
			ps.executeUpdate();
			
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
		finally  {
			if (ps != null) {try{ ps.close(); } catch (Exception ex){}};
			if (con != null) {try{ con.close(); } catch (Exception ex){}};
		}
		
	}

}
