package impl.tew.business.resteasy;

import impl.tew.business.SimpleAmigoService;

import java.util.List;

import com.tew.business.resteasy.AmigoServicesRs;
import com.tew.model.Usuario;

public class AmigoServicesRsImpl implements AmigoServicesRs{

	@Override
	public void creaInvi(String emailU, String emailA) {
		new SimpleAmigoService().creaInvi(emailU,emailA);		
	}

	@Override
	public List<Usuario> InvisRecibidas(String emailU) {
		return new SimpleAmigoService().InvisRecibidas(emailU);
	}

	@Override
	public List<Usuario> AmigosUsuario(String emailU) {
		return new SimpleAmigoService().AmigosUsuario(emailU);
	}

	@Override
	public void aceptaInvi(String emailU, String emailA) {
		new SimpleAmigoService().aceptaInvi(emailU, emailA);
		
	}

	@Override
	public List<Usuario> NoAmigosUsuario(String emailU) {
		return new SimpleAmigoService().NoAmigosUsuario(emailU);
	}

}
