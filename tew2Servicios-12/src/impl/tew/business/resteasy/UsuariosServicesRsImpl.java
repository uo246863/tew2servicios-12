package impl.tew.business.resteasy;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;

import com.tew.business.exception.EntityAlreadyExistsException;
import com.tew.business.exception.EntityNotFoundException;
import com.tew.business.resteasy.UsuariosServicesRs;
import com.tew.model.Usuario;

import impl.tew.business.SimpleUsuariosService;

public class UsuariosServicesRsImpl implements UsuariosServicesRs {
	
	@Context
	ServletContext context;

	@Override
	public Usuario findById(String string) throws EntityNotFoundException {
		return new SimpleUsuariosService().findById(string);
	}

	@Override
	public boolean deleteUsuario(String string) throws EntityNotFoundException {
		
		//Lista que contendrá los usuarios en sesion
		ArrayList<String> usersEnSesion;
		//Cargamos del atributo del contexto los usuarion en sesion
		usersEnSesion = (ArrayList<String>) context.getAttribute("UsersInSession");
		//Comprobamos si el usuario a borrar se encuentra en sesion
		if (usersEnSesion.contains(string)) {
			//Retornamos falso ya que no se puede borrar
			return false;
		}
		else {
			return new SimpleUsuariosService().deleteUsuario(string);
		}			
	}

	@Override
	public List<Usuario> getUsuarios() throws Exception {
		return new SimpleUsuariosService().getUsuarios();
	}

	@Override
	public void saveUsuario(Usuario Usuario) throws EntityAlreadyExistsException {
		new SimpleUsuariosService().saveUsuario(Usuario);
	}



	@Override
	public void reiniciarBD() {
		new SimpleUsuariosService().reiniciarBD();
		
	}

	@Override
	public void editNombreUsuario(String email, String nombre)
			throws EntityNotFoundException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void editRolUsuario(String email, String rol)
			throws EntityNotFoundException {
		// TODO Auto-generated method stub
		
	}

}
