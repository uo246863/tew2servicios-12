package impl.tew.business.resteasy;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;

import impl.tew.business.SimpleLoginService;

import com.tew.business.exception.EntityNotFoundException;
import com.tew.business.resteasy.LoginServicesRs;
import com.tew.model.Usuario;

public class LoginServicesRsImpl implements LoginServicesRs {

	@Context
	ServletContext context;

	@Override
	public Usuario verify(String login, String passwd)
			throws EntityNotFoundException {

		Usuario user = new SimpleLoginService().verify(login, passwd);

		if (user != null) {
			putUserInSession(user.getEmail());
		}

		return user;
	}

	private void putUserInSession(String email) {
		//Lista que contendrá los usuarios en sesion
		ArrayList<String> usersEnSesion;

		// Cargamos del atributo del contexto los que esten en sesion en caso de
		// haber
		if (context.getAttribute("UsersInSession") != null) {
			usersEnSesion = (ArrayList<String>) context.getAttribute("UsersInSession");
		}
		//No hay usuarios
		else {
			usersEnSesion = new ArrayList<String>();
		}
		
		//Añadimos el usuario al contexto solo si no esta en la lista
		if (!usersEnSesion.contains(email)) {
			usersEnSesion.add(email);
		}
		context.setAttribute("UsersInSession", usersEnSesion);
		
		System.out.println("En sesion tras llamada a login:" + usersEnSesion.toString());
	}

	@Override
	public void logout(String email) {
		
		//Lista que contendrá los usuarios en sesion
		ArrayList<String> usersEnSesion;
		//Cargamos del atributo del contexto los usuarion en sesion
		usersEnSesion = (ArrayList<String>) context.getAttribute("UsersInSession");
		//Retiramos el email de la lista y actualizamos el contexto
		usersEnSesion.remove(email);
		context.setAttribute("UsersInSession", usersEnSesion);
		
		System.out.println("En sesion tras llamada a logout:" + usersEnSesion.toString());
		
	}
	
	

}
