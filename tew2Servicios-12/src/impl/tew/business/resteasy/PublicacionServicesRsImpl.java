package impl.tew.business.resteasy;

import impl.tew.business.SimplePublicacionService;

import java.util.List;

import com.tew.business.resteasy.PublicacionServicesRs;
import com.tew.model.Publicacion;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
public class PublicacionServicesRsImpl implements PublicacionServicesRs{

	@Context ServletContext context;
	@Override
	public List<Publicacion> getPublicaciones() throws Exception {
		return new SimplePublicacionService().getPublicaciones(); 
	}

	@Override
	public void crearPublicacion(String titulo, String texto, String email) {
		new SimplePublicacionService().crearPublicacion(titulo,texto,email);
	}

	@Override
	public List<Publicacion> getPublicacionesAmigos(String email) throws Exception {
		return new SimplePublicacionService().getPublicacionesAmigos(email);
	}

	@Override
	public List<Publicacion> getMisPublicaciones(String email) throws Exception {		
		return new SimplePublicacionService().getMisPublicaciones(email);
	}
	

	@Override
	public void borrarPublicacion(String texto, String titulo, String fecha, String email) {
		new SimplePublicacionService().borrarPublicacion(texto, titulo, fecha, email);
		
	}


}
