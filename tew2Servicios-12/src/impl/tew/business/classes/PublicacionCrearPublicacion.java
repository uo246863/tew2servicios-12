package impl.tew.business.classes;

import java.util.List;

import com.tew.infrastructure.Factories;
import com.tew.model.Publicacion;
import com.tew.model.Usuario;
import com.tew.persistence.PublicacionDao;
import com.tew.persistence.UsuarioDao;

public class PublicacionCrearPublicacion {

	

	public void crearPublicacion(String titulo, String texto, String email){
		PublicacionDao dao = Factories.persistence.createPublicacionDao();
		dao.crearPublicacion(titulo, texto, email);
		
	}
	
	public  List<Publicacion> getPublicaciones(){
		PublicacionDao dao = Factories.persistence.createPublicacionDao();
		return dao.getPublicaciones();
		
	}
}
