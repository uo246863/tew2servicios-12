package impl.tew.business.classes;

import java.util.List;

import com.tew.infrastructure.Factories;
import com.tew.model.Publicacion;
import com.tew.persistence.PublicacionDao;

public class PublicacionListadoMias {

	public List<Publicacion> getMisPublicaciones(String email) throws Exception {
		
		PublicacionDao dao = Factories.persistence.createPublicacionDao();
		return  dao.getMisPublicaciones(email);

	}
	
	public void borrarPublicacion(String texto, String titulo, String fecha, String email){
		PublicacionDao dao = Factories.persistence.createPublicacionDao();
		dao.borrarPublicacion(texto, titulo, fecha, email);
	}
}
