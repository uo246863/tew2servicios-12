package impl.tew.business.classes;

import java.util.List;

import com.tew.infrastructure.Factories;
import com.tew.model.Usuario;
import com.tew.persistence.AmigoDao;

public class AmigoInvitacion {
	
	public void crearInvitacion(String email_u, String email_a){
		AmigoDao dao = Factories.persistence.createAmigoDao();
		dao.creaInvi(email_u, email_a);
		
	}
	
	public List<Usuario> InvisRecibidas(String email_u) {
		AmigoDao dao = Factories.persistence.createAmigoDao();
		return dao.InvisRecibidas(email_u);
	}
	
	public List<Usuario> AmigosUsuario(String email_u) {
		AmigoDao dao = Factories.persistence.createAmigoDao();
		return dao.AmigosUsuario(email_u);
	}
	
	
	public void aceptaInvitacion(String email_u, String email_a){
		AmigoDao dao = Factories.persistence.createAmigoDao();
		dao.aceptaInvi(email_u, email_a);
		
	}

	public List<Usuario> NoAmigosUsuario(String emailU) {
		AmigoDao dao = Factories.persistence.createAmigoDao();
		return dao.NoAmigosUsuario(emailU);
	}
	

}
