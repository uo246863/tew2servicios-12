package impl.tew.business.classes;

import com.tew.business.exception.EntityNotFoundException;
import com.tew.infrastructure.Factories;
import com.tew.model.Usuario;
import com.tew.persistence.UsuarioDao;
import com.tew.persistence.exception.NotPersistedException;

public class UsuariosUpdate {

	public void editNombre(String email, String nombre) throws EntityNotFoundException {
		UsuarioDao dao = Factories.persistence.createUsuarioDao();
		dao.editNombre(email,nombre);
	}
	
	public void editRol(String email, String rol) throws EntityNotFoundException {
		UsuarioDao dao = Factories.persistence.createUsuarioDao();
		dao.editRol(email,rol);
	}

}
