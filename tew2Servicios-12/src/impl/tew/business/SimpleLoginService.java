package impl.tew.business;

import java.util.Iterator;
import java.util.List;

import com.tew.business.LoginService;
import com.tew.business.exception.EntityNotFoundException;

import impl.tew.business.classes.UsuariosBuscar;
import impl.tew.business.classes.UsuariosListado;

import com.tew.model.Usuario;

public class SimpleLoginService implements LoginService {

	@Override
	public Usuario verify(String email, String password) {

		Usuario user = null;
		if (!validLogin(email, password)) {
			return user;
		} else {
			UsuariosBuscar aux = new UsuariosBuscar();
			try {
				user = aux.find(email);
			} catch (EntityNotFoundException e) {
				e.printStackTrace();
			}
			

			return user;

		}
	}

	private boolean validLogin(String email, String password) {

		UsuariosListado listar = new UsuariosListado();
		List<Usuario> usuarios;
		try {
			usuarios = listar.getUsuarios();
			Iterator<Usuario> itr = usuarios.iterator();
			while (itr.hasNext()) {
				Usuario current = itr.next();
				if (current.getEmail().equals(email) && current.getPasswd().equals(password)) {
					return true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

	@Override
	public void logout(String email) {
		
	}

	
}

