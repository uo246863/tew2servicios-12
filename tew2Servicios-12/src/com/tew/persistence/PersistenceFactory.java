package com.tew.persistence;

import com.tew.persistence.UsuarioDao;

public interface PersistenceFactory {
	
	UsuarioDao createUsuarioDao();
	PublicacionDao createPublicacionDao();
	AmigoDao createAmigoDao();
	// ... otros m��todos factoria para Daos de otras entidades del modelo ...
}

