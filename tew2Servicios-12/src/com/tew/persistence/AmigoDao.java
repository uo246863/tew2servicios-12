package com.tew.persistence;

import java.util.List;

import com.tew.model.Usuario;

public interface AmigoDao {
	
	void creaInvi(String emailU, String emailA);
	List<Usuario> InvisRecibidas(String emailU);
	List<Usuario> AmigosUsuario(String email_u);
	void aceptaInvi(String emailU, String emailA);
	List<Usuario> NoAmigosUsuario(String emailU);

}
