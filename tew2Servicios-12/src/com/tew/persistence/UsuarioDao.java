package com.tew.persistence;

import java.util.List;

import com.tew.model.Usuario;
import com.tew.persistence.exception.AlreadyPersistedException;
import com.tew.persistence.exception.NotPersistedException;


public interface UsuarioDao {

	List<Usuario> getUsuarios();
	void save(Usuario a) throws AlreadyPersistedException;
	boolean delete(String id) throws NotPersistedException;
	Usuario findById(String id);
	void editNombre(String email, String nombre);
	void editRol(String email, String rol);
	void reiniciarBD();
	void crearPublicacion(String titulo, String texto, String email);

}