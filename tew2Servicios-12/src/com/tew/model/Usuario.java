package com.tew.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//Anotamos en la clase DTO los tipos de datos estructurados que se van a emplear

@XmlRootElement(name = "usuario")
public class Usuario {
	
	private static final long serialVersionUID = 6793L;
	private String email;
	private String passwd;
	private String rol;
	private String nombre;
	
	public Usuario(String correo) {
		this.email = correo;
	}
	
	public Usuario() {
	}

	public Usuario(String correo, String contra, String role, String name) {
		this.email = correo;
		this.passwd = contra;
		this.rol = role;
		this.nombre = name;
	}

	@XmlElement
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@XmlElement
	public String getPasswd() {
		return passwd;
	}
	
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	@XmlElement
	public String getRol() {
		return rol;
	}
	
	public void setRol(String rol) {
		this.rol = rol;
	}
	
	@XmlElement
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	 
	
	
}
