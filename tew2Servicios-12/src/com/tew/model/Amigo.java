package com.tew.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//Anotamos en la clase DTO los tipos de datos estructurados que se van a emplear

@XmlRootElement(name = "amigo")
public class Amigo {
	
	private String email_usuario;
	private String email_amigo;
	private boolean aceptada;
	
	
	public Amigo(String email_usuario, String email_amigo) {
		this.email_usuario = email_usuario;
		this.email_amigo = email_amigo;
		this.aceptada = false;
	}
	
	@XmlElement
	public String getEmail_usuario() {
		return email_usuario;
	}
	
	public void setEmail_usuario(String email_usuario) {
		this.email_usuario = email_usuario;
	}
	
	@XmlElement
	public String getEmail_amigo() {
		return email_amigo;
	}
	
	public void setEmail_amigo(String email_amigo) {
		this.email_amigo = email_amigo;
	}
	
	@XmlElement
	public boolean isAceptada() {
		return aceptada;
	}
	public void setAceptada(boolean aceptada) {
		this.aceptada = aceptada;
	}
	
	

}
