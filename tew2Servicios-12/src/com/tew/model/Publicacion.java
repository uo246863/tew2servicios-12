package com.tew.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//Anotamos en la clase DTO los tipos de datos estructurados que se van a emplear

@XmlRootElement(name = "publicacion")
public class Publicacion implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String email;
	private String titulo;
	private String texto;
	private String fecha;
	
	public Publicacion(String correo) {
		this.email = correo;
	}
	
	public Publicacion() {
	}
	
	@XmlElement
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@XmlElement
	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	@XmlElement
	public String getTexto() {
		return texto;
	}
	
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	@XmlElement
	public String getFecha() {
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
}
