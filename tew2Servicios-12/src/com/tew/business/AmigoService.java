package com.tew.business;

import java.util.List;

import com.tew.model.Usuario;


public interface AmigoService {
	
	void creaInvi(String emailU, String emailA);
	List<Usuario> InvisRecibidas(String emailU);
	List<Usuario> AmigosUsuario(String emailU);
	void aceptaInvi(String emailU, String emailA);

}
