package com.tew.business;
import com.tew.business.exception.EntityNotFoundException;
import com.tew.model.Usuario;

public interface LoginService {
	
	Usuario verify(String login, String password) throws EntityNotFoundException;
	void logout(String email);
	}
