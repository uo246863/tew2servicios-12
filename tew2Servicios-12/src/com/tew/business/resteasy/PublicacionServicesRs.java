package com.tew.business.resteasy;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tew.business.PublicacionService;
import com.tew.model.Publicacion;

//Esta será la fachada de los servicios REST para Publicacion

@Path("/PublicacionServicesRs")
public interface PublicacionServicesRs extends PublicacionService {
	
	 @GET
	 @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	 List<Publicacion> getPublicaciones() throws Exception;
	 
	 @PUT
	 @Path("CrearPubli/{titulo}/{texto}/{email}")
	 @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	 void crearPublicacion(@PathParam("titulo") String titulo, @PathParam("texto") String texto, @PathParam("email") String email);
	 
	 @GET
	 @Path("PublisAmigos/{emailU}")
	 @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	 List<Publicacion> getPublicacionesAmigos(@PathParam("emailU") String emailU) throws Exception;
	 
	 @GET
	 @Path("MisPublis/{emailU}")
	 @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	 List<Publicacion> getMisPublicaciones(@PathParam("emailU") String emailU) throws Exception;
	 
	 @DELETE
	 @Path("borraPubli/{titulo}/{texto}/{fecha}/{email}")
	 @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	 void borrarPublicacion(@PathParam("titulo") String titulo, @PathParam("texto") String texto, @PathParam("fecha") String fecha,@PathParam("email") String email);
}
