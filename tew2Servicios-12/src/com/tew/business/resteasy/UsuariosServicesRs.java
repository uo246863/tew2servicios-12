package com.tew.business.resteasy;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tew.business.UsuariosService;
import com.tew.business.exception.EntityAlreadyExistsException;
import com.tew.business.exception.EntityNotFoundException;
import com.tew.model.Usuario;

//Esta será la fachada de los servicios REST para Usuarios
@Path("/UsuariosServicesRs")
public interface UsuariosServicesRs extends UsuariosService{
	
	   @GET
	   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	   public List<Usuario> getUsuarios() throws Exception;
		
	   @GET
	   @Path("{email}")
	   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	   Usuario findById(@PathParam("email") String email) throws EntityNotFoundException;
		
	   @GET
	   @Path("/borraUsuario/{email}")
	   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	   @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	   boolean deleteUsuario(@PathParam("email") String email) throws EntityNotFoundException;

	   @PUT
	   @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	   void saveUsuario(Usuario Usuario) throws EntityAlreadyExistsException;
		
	   
	   @PUT
	   @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	   void reiniciarBD();
	   
	  
}
