package com.tew.business.resteasy;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tew.business.AmigoService;
import com.tew.model.Usuario;

//Esta será la fachada de los servicios REST para Publicacion

@Path("/AmigoServicesRs")
public interface AmigoServicesRs extends AmigoService{

	@PUT
	@Path("/{emailU}/{emailA}")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	void creaInvi(@PathParam("emailU") String emailU, @PathParam("emailA") String emailA);
	
	@GET
	@Path("/noAmigos/{emailUser}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	List<Usuario> NoAmigosUsuario(@PathParam("emailUser") String emailUser);
	
	@GET
	@Path("{emailU}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	List<Usuario> InvisRecibidas(@PathParam("emailU") String emailU);
	
	
	@POST
	@Path("aceptaInvi/{emailU}/{emailA}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	void aceptaInvi(@PathParam("emailU") String emailU, @PathParam("emailA") String emailA);
	
}
