package com.tew.business.resteasy;

import impl.tew.business.resteasy.AmigoServicesRsImpl;
import impl.tew.business.resteasy.LoginServicesRsImpl;
import impl.tew.business.resteasy.PublicacionServicesRsImpl;
import impl.tew.business.resteasy.UsuariosServicesRsImpl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("unchecked")
public class Application extends javax.ws.rs.core.Application {

	private Set<Class<?>> classes = new HashSet<Class<?>>();

	public Application() {

		classes.add(LoginServicesRsImpl.class);
		classes.add(PublicacionServicesRsImpl.class);
		classes.add(AmigoServicesRsImpl.class);
		classes.add(UsuariosServicesRsImpl.class);
		
	}
 	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}
	
	@Override
	public Set<Object> getSingletons() {
		return Collections.EMPTY_SET;
	} 

}
