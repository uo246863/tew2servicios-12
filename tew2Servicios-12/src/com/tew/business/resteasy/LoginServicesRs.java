package com.tew.business.resteasy;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tew.business.LoginService;
import com.tew.business.exception.EntityNotFoundException;
import com.tew.model.Usuario;

@Path("/LoginServicesRs")
public interface LoginServicesRs extends LoginService {
	
	 @GET
	 @Path("{login}/{passwd}")
	 @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	 public Usuario verify(@PathParam("login") String login, @PathParam("passwd") String passwd) throws EntityNotFoundException;
	 
	 @PUT
	 @Path("{emailU}")
	 @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	 void logout(@PathParam("emailU") String email);
	 
}
