package com.tew.business;

import java.util.List;

import com.tew.model.Publicacion;



public interface PublicacionService {

	List<Publicacion> getPublicaciones() throws Exception;
	void crearPublicacion(String titulo, String texto, String email);
	List<Publicacion> getPublicacionesAmigos(String email) throws Exception;
	List<Publicacion> getMisPublicaciones(String email) throws Exception;
	void borrarPublicacion(String texto, String titulo, String fecha, String email);
}
