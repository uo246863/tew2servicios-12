package com.tew.presentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tew.business.UsuariosService;
import com.tew.infrastructure.Factories;
import com.tew.model.Usuario;

@ManagedBean
public class BeanUsuarios implements Serializable {
	private static final long serialVersionUID = 55555L;
	// Se añade este atributo de entidad para recibir el Usuario concreto
	// selecionado de la tabla o de un formulario
	// Es necesario inicializarlo para que al entrar desde el formulario de
	// AltaForm.xml se puedan
	// dejar los avalores en un objeto existente.

	private Usuario[] Usuarios = null;

	// uso de inyección de dependencia
	@ManagedProperty(value = "#{Usuario}")
	private BeanUsuario Usuario;
	private List<Usuario> usuariosFiltrados = null;

	public BeanUsuario getUsuario() {
		return Usuario;
	}

	public void setUsuario(BeanUsuario Usuario) {
		this.Usuario = Usuario;
	}

	private Map<Usuario, Boolean> checked = new HashMap<Usuario, Boolean>();

	/*
	 * public BeanUsuarios() { iniciaUsuario(null); }
	 */

	public Map<Usuario, Boolean> getChecked() {
		return checked;
	}

	public List<Usuario> getUsuariosFiltrados() {
		return usuariosFiltrados;
	}

	public void setUsuariosFiltrados(List<Usuario> usuariosFiltrados) {
		this.usuariosFiltrados = usuariosFiltrados;
	}

	public void setChecked(Map<Usuario, Boolean> checked) {
		this.checked = checked;
	}

	public Usuario[] getUsuarios() {
		return (Usuarios);
	}

	public void setUsuarios(Usuario[] Usuarios) {
		this.Usuarios = Usuarios;
	}

	public void iniciaUsuario(ActionEvent event) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		// Obtenemos el archivo de propiedades correspondiente al idioma que
		// tengamos seleccionado y que viene envuelto en facesContext
		ResourceBundle bundle = facesContext.getApplication()
				.getResourceBundle(facesContext, "msgs");

		Usuario.setEmail("valorDefectoUserEmail"); // null?
		Usuario.setPasswd(bundle.getString("valorDefectoUserPasswd"));
		Usuario.setRol(bundle.getString("valorDefectoRol"));
		Usuario.setNombre(bundle.getString("valorDefectoNombre"));
	}

	public String reinicioBD() {
		UsuariosService service;
		try {
			service = Factories.services.createUsuariosService();
			service.reiniciarBD();
			FacesContext jsfCtx = FacesContext.getCurrentInstance();
			ResourceBundle bundle = jsfCtx.getApplication().getResourceBundle(
					jsfCtx, "msgs");
			FacesMessage msg = new FacesMessage(FacesMessage.FACES_MESSAGES,
					bundle.getString("exito_reinicioBD"));
			FacesContext.getCurrentInstance().addMessage(null, msg);

			return "exito";

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	public String listado() {
		UsuariosService service;
		try {
			// Acceso a la implementacion de la capa de negocio
			// a trav��s de la factor��a
			service = Factories.services.createUsuariosService();
			// De esta forma le damos informaci��n a toArray para poder hacer el
			// casting a Usuario[]
			Usuarios = (Usuario[]) service.getUsuarios()
					.toArray(new Usuario[0]);
			return "exito";

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}
	
	public String listado2() {
		UsuariosService service;
		try {
			// Acceso a la implementacion de la capa de negocio
			// a trav��s de la factor��a
			service = Factories.services.createUsuariosService();
			// De esta forma le damos informaci��n a toArray para poder hacer el
			// casting a Usuario[]
			Usuarios = (Usuario[]) service.getUsuarios()
					.toArray(new Usuario[0]);
			return "exito";

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	public String baja(Usuario Usuario) {
		UsuariosService service;
		FacesContext jsfCtx = FacesContext.getCurrentInstance();
		ResourceBundle bundle = jsfCtx.getApplication().getResourceBundle(
				jsfCtx, "msgs");
		try {
			// Acceso a la implementacion de la capa de negocio
			// a trav��s de la factor��a
			service = Factories.services.createUsuariosService();
			// Comprobamos si esta en sesion
			ArrayList<Usuario> usuarios_activos = (ArrayList<Usuario>) jsfCtx.getExternalContext().getApplicationMap().get("USUARIOS");
			Iterator<Usuario> itr = usuarios_activos.iterator();
			while (itr.hasNext()) {
				if (itr.next().getEmail().equals(Usuario.getEmail())) {
					FacesMessage msg = new FacesMessage(
							FacesMessage.SEVERITY_WARN,
							bundle.getString("baja_form_sesion_error"), null);
					FacesContext.getCurrentInstance().addMessage(null, msg);
					return "error";
				}

			}
			service.deleteUsuario(Usuario.getEmail());
			// Actualizamos el javabean de Usuarios inyectado en la tabla.
			Usuarios = (Usuario[]) service.getUsuarios()
					.toArray(new Usuario[0]);
			return "exito";

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	public String deleteV() {
		UsuariosService service;
		FacesContext jsfCtx = FacesContext.getCurrentInstance();
		ResourceBundle bundle = jsfCtx.getApplication().getResourceBundle(
				jsfCtx, "msgs");
		try {
			// Acceso a la implementacion de la capa de negocio
			// a trav��s de la factor��a
			service = Factories.services.createUsuariosService();
			ArrayList<Usuario> usuarios_activos = (ArrayList<Usuario>) jsfCtx
					.getExternalContext().getApplicationMap()
					.get("USUARIOS");
			List<Usuario> usuariosABorrar = new ArrayList<Usuario>();

			for (Usuario user : Usuarios) {
				if (checked.get(user)) {
					//Comprobamos que no este en sesion
					Iterator<Usuario> itr = usuarios_activos.iterator();
					while (itr.hasNext()) {
						if (itr.next().getEmail().equals(user.getEmail())) {
							FacesMessage msg = new FacesMessage(
									FacesMessage.SEVERITY_WARN,
									bundle.getString("baja_form_sesion_error2"), null);
							FacesContext.getCurrentInstance().addMessage(null, msg);
							break;
						}
					}
					if (!itr.hasNext()) {
						usuariosABorrar.add(user);
					}
				}
			}
			// Recorremos el ArrayList
			Iterator<Usuario> itr = usuariosABorrar.iterator();
			while (itr.hasNext()) {
				service.deleteUsuario(itr.next().getEmail());
			}
			checked.clear();
			Usuarios = (Usuario[]) service.getUsuarios()
					.toArray(new Usuario[0]);
			return "exito";

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	public String edit(Usuario[] usuarios) {
		UsuariosService service;
		try {
			// Acceso a la implementacion de la capa de negocio
			// a trav��s de la factor��a
			service = Factories.services.createUsuariosService();
			// Recargamos el Usuario seleccionado en la tabla de la base de
			// datos por si hubiera cambios.
			// MIRAR
			
			Usuario[] Usuarios_BD = (Usuario[]) service.getUsuarios()
					.toArray(new Usuario[0]);
			
			for (int i = 0; i < usuarios.length; i++) {
				if (Usuarios_BD[i].getNombre() != usuarios[i].getNombre()) {
					service.editNombreUsuario(usuarios[i].getEmail(), usuarios[i].getNombre());
				}
				if (Usuarios_BD[i].getRol() != usuarios[i].getRol()) {
					service.editRolUsuario(usuarios[i].getEmail(), usuarios[i].getRol());
				}
					
			}
			
			return "exito";

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}

	// Se inicia correctamente el MBean inyectado si JSF lo hubiera crea
	// y en caso contrario se crea. (hay que tener en cuenta que es un Bean de
	// sesión)
	// Se usa @PostConstruct, ya que en el contructor no se sabe todavía si el
	// Managed Bean
	// ya estaba construido y en @PostConstruct SI.
	@PostConstruct
	public void init() {
		System.out.println("BeanUsuarios - PostConstruct");
		// Buscamos el Usuario en la sesión. Esto es un patrón factoría
		// claramente.
		Usuario = (BeanUsuario) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap()
				.get(new String("Usuario"));
		// si no existe lo creamos e inicializamos
		if (Usuario == null) {
			System.out.println("BeanUsuarios - No existia");
			Usuario = new BeanUsuario();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("Usuario", Usuario);
		}
	}

	@PreDestroy
	public void end() {
		System.out.println("BeanUsuarios - PreDestroy");
	}

}