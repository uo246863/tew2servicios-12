package com.tew.presentation;
import java.io.Serializable;
import java.util.ResourceBundle;

import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.tew.model.Usuario;

@ManagedBean(name="Usuario")
@SessionScoped
public class BeanUsuario extends Usuario implements Serializable {
	private static final long serialVersionUID = 55556L;
	
	public BeanUsuario() {
		iniciaUsuario(null);
	}
//Este método es necesario para copiar el Usuario a editar cuando
//se pincha el enlace Editar en la vista listado.xhtml. Podría sustituirse 
//por un método editar en BeanUsuarios.
	public void setUsuario(Usuario Usuario) {
		setEmail(Usuario.getEmail());
		setPasswd(Usuario.getPasswd());
		setRol(Usuario.getRol());
		setNombre(Usuario.getNombre());
		
	}
//Iniciamos los datos del Usuario con los valores por defecto 
//extraídos del archivo de propiedades correspondiente
    public void iniciaUsuario(ActionEvent event) {
	    FacesContext facesContext = FacesContext.getCurrentInstance();
    	    ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msgs");
    	    setEmail(bundle.getString("valorDefectoCorreo"));
    	    setPasswd(bundle.getString("valorDefectoPasswd"));
    	    setRol(bundle.getString("valorDefectoRol"));
    	    setNombre(bundle.getString("valorDefectoNombre"));
	  }	      
} 
