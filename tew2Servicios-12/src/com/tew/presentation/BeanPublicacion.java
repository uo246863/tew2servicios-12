package com.tew.presentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tew.business.PublicacionService;
import com.tew.infrastructure.Factories;
import com.tew.model.Publicacion;


@ManagedBean
public class BeanPublicacion implements Serializable{

	private Publicacion[] Publicaciones = null;
	private List<Publicacion> PublicacionesL=null;
	private List<Publicacion> PublicacionesAmigos=null;
	
	public Publicacion[] getPublicaciones () {
	    return(Publicaciones);
	  }
	public void setPublicaciones(Publicacion[] Publicaciones) {
		  this.Publicaciones = Publicaciones;
   } 
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	private static final long serialVersionUID = -6238217336303198667L;
    private String title="";
    private String text="";
	private String fecha="";
	

	 public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String crearPublicacion(){
	  
	   PublicacionService service;
  	   service = Factories.services.createPublicacionService();
  	   FacesContext context = FacesContext.getCurrentInstance();
  	   HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
  	   HttpSession httpSession = request.getSession(false);
  	   String email = (String) httpSession.getAttribute("Email");
  	   service.crearPublicacion(title, text, email);
  	  
	  	   
	   return "exito";
  	  	   
     }
	
	public String listado() {
	       PublicacionService service;
			  try {
				service = Factories.services.createPublicacionService();
				Publicaciones = (Publicacion [])service.getPublicaciones().toArray(new Publicacion[0]);			
				setPublicacionesL(Arrays.asList(Publicaciones));
				return "exito";
				
			  } catch (Exception e) {
				e.printStackTrace();  
				return "error";
			  }
			  
	 	  }
	
	public String listadoMisPublicaciones(){
		PublicacionService service;
		  try {
			service = Factories.services.createPublicacionService();
			Publicaciones = (Publicacion [])service.getMisPublicaciones(fecha).toArray(new Publicacion[0]);			
			setPublicacionesL(Arrays.asList(Publicaciones));
			return "exito";
			
		  } catch (Exception e) {
			e.printStackTrace();  
			return "error";
		  }
	}
	public List<Publicacion> getPublicacionesL() {
		return PublicacionesL;
	}
	public void setPublicacionesL(List<Publicacion> publicacionesL) {
		PublicacionesL = new ArrayList<Publicacion>();
		FacesContext context = FacesContext.getCurrentInstance();
	    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
	    HttpSession httpSession = request.getSession(false);
	  	String email = (String) httpSession.getAttribute("Email");
		for (Publicacion p : publicacionesL){
			if (p.getEmail().equals(email)){
				this.PublicacionesL.add(p);
			}
		}
		
	}
	public List<Publicacion> getPublicacionesAmigos() {
		return PublicacionesAmigos;
	}
	public String listadoPublicacionesAmigos() {
		PublicacionService service;
		  try {
			service = Factories.services.createPublicacionService();
			Publicaciones = (Publicacion [])service.getPublicacionesAmigos(fecha).toArray(new Publicacion[0]);			
			setPublicacionesAmigos(Arrays.asList(Publicaciones));	
			return "exito";
		  } catch (Exception e) {
			e.printStackTrace();
			return "error";
		  }
	}
	
	public void setPublicacionesAmigos(List<Publicacion> publicacionesAmigos) {
			this.PublicacionesAmigos=publicacionesAmigos;
	}
}
