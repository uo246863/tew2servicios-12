package com.tew.presentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tew.business.LoginService;
import com.tew.business.exception.EntityNotFoundException;
import com.tew.infrastructure.Factories;
import com.tew.model.Usuario;

@ManagedBean
public class BeanLogin implements Serializable {
	
	private static final long serialVersionUID = 55597L;
	public BeanUsuario Usuario;
	private String email = "";
	private String password = "";
	public String getEmail() {
		return email;
	}
	public void setEmail(String name) {
		this.email = name;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
	 
	public String verify() throws EntityNotFoundException {
		// necesario para accede a msgs y a los mensajes en español e ingles de
		// los ficheros
		// de propiedades
		FacesContext jsfCtx = FacesContext.getCurrentInstance();
		ResourceBundle bundle = jsfCtx.getApplication().getResourceBundle(
				jsfCtx, "msgs");
		FacesMessage msg = null;
		LoginService login = Factories.services.createLoginService();

		Usuario user = login.verify(email, password);
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		HttpSession httpSession = request.getSession(false);
		httpSession.setAttribute("Email", email);
		if (user != null) {
			httpSession.setAttribute("Rol", user.getRol());
			putUserInSession(user);
			
			
			if (user.getRol().equals("admin")) {				
				return "admin";
			}

			else {
				return "user";
			}
		}
		// si el usuario no se encuentra
		// se prepara el mensaje que saldra en la vista del cliente
		msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
				bundle.getString("login_form_result_error"), null);
		// se añade al element con id=”msg”
		FacesContext.getCurrentInstance().addMessage(null, msg);
		return "login";
	}

	public BeanUsuario getUsuario() {
		return Usuario;
	}
	public void setUsuario(BeanUsuario usuario) {
		Usuario = usuario;
	}
	@SuppressWarnings("unchecked")
	public void putUserInSession(Usuario usuario) {
		Map<String, Object> session = FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap();
		session.put("LOGGEDIN_USER", usuario);
		
		List<Usuario> usuarios_activos = new ArrayList<Usuario>();
		//Si ya hay una lista activa la cargamos, si no la creamos
		if (FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().containsKey("USUARIOS")){
			usuarios_activos = (List<Usuario>) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("USUARIOS");
			usuarios_activos.add(usuario);
			FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().put("USUARIOS", usuarios_activos);
		}
		else {
			usuarios_activos.add(usuario);
			FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().put("USUARIOS", usuarios_activos);
		}
		
		
	}
	
	public String logout() {
		
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "exito";
	}

}
