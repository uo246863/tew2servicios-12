//Clase que contiene el Modelo de la aplicación. 
function Model() {
	this.tbUsuarios=null;
}

Model.prototype = {
	//Reinicia la base de datos
	loadBD: function(){
		UsuariosServicesRs.reiniciarBD();
	},
	//Obtenemos los usuarios
	load : function() {
		this.tbUsuarios = UsuariosServicesRs.getUsuarios();
	},
	
	// Eliminación un usuario existente
	remove : function(correo) {
		// Llamamos al servicio de borrado de usuario
		UsuariosServicesRs.deleteUsuario({
			email : correo
		});
		// Recargamos la lista de usuarios.
		this.load();
	}
	
};

//Clase que contiene la gestión de la capa Vista
function View(model) {
	// referencia al modelo
	this.model = model;
}

View.prototype = {
		
		list : function() {

			$("#tblList").html("");
			$("#tblList").html("<thead>" + "<tr>" + "<th></th>"
				+ "<th>Email</th>" + "<th>Nombre</th>" + "<th>Seleccionar varios</th>"
				+ "</tr>" + "</thead>" + "<tbody>" + "</tbody>");
			for ( var i in this.model.tbUsuarios) {
				var usuario = this.model.tbUsuarios[i];
				$("#tblList tbody").append(
					"<tr><td><img src='icons/delete.png' class='btnDelete'/></td>" 
					+ "<td>" + usuario.email
					+ "</td>" + "<td>" + usuario.nombre + "</td>" + 
					"<td><input type='checkbox' id='cbox'" + i + "value='select'></td></tr>");
			}
		},
		
		remove : function(celda) {
			
			// Accedemos a la fila que está por encima de esta celda (closest('tr'))
			// y despues obtenemos todas las celdas de esa fila (find('tr')) y
			// nos quedamos con la segunda (get(1)) que es la contiene el "email" del
			// usuario. Al ser un string no requerimos parsearlo
			correo = celda.closest('tr').find('td').get(1).innerHTML;
			
			//Lo pasamos al modelo para que lo borre
			this.model.remove(correo);
		} 
};

$(function() {
	//Creamos el modelo con los datos y la conexión al servicio web.
	var model = new Model();
	//Creamos la vista que incluye acceso al modelo.
	var view = new View(model);
	
	model.loadBD();
	//view.list();
	/*
	//Manejador para el evento de delete
	$("#tblList").on("click", ".btnDelete", function(event) {
		//Ordena borrar el usuario y cargar la nueva lista
		view.remove($(this));
		view.list();
	});
	
	$("#reinicioBD").on("click", function(event) {
		//Llamamos a la función de reinicio de BD:
		alert("Reiniciando base de datos.");   
		model.loadBD();
		window.location.href = "opciones-admin.html";
		
	});
	*/
});
